<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('posts')->insert([
			[
	            'author' => 'admin',
	            'name' => 'My first post',
	            'content' => 'Hello world!',
	            'image' => 'pic1.png',
	            'created_at' => '2019-11-26 20:25:35',
	        ],
			[
	            'author' => 'admin',
	            'name' => 'My second post',
	            'content' => 'Hello world!',
	            'image' => 'pic2.png',
	            'created_at' => '2019-11-26 20:25:35',
	        ]
	    ]);
    }
}
