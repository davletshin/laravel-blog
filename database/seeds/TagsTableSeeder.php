<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tags')->insert([
			[
	            'name' => 'News',
	        ],
			[
	            'name' => 'Articles',
	        ],
	    ]);
		DB::table('post_tag')->insert([
			[
	            'post_id' => 1,
	            'tag_id' => 1,
	        ],
			[
	            'post_id' => 2,
	            'tag_id' => 1,
	        ],
			[
	            'post_id' => 2,
	            'tag_id' => 2,
	        ],
	    ]);
    }
}
