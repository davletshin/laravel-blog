<!DOCTYPE html>
<html>
<head>
	<title>Posts</title>
</head>
<body>
	<h1>Posts</h1>
	@foreach ($posts as $post)
		<img src="{{ $post->image }}">
		<h2>{{ $post->name }}</h2>
		<div class="author">{{ $post->author }} - {{ $post->created_at }}</div>
		<div>{!! explode('<!-- more -->', $post->content)[0] !!}<a href="/posts/{{ $post->id }}">more...</a></div>
	@endforeach
</body>
</html>
