<!DOCTYPE html>
<html>
<head>
	<title>Posts</title>
</head>
<body>
	<img src="{{ $post->image }}">
	<h1>{{ $post->name }}</h1>
	<div class="author">{{ $post->author }} - {{ $post->created_at }}</div>
	<div>{!! $post->content !!}</div>

</body>
</html>
